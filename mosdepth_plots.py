import pandas as pd
import matplotlib.pyplot as plt


def plot_coverage_vs_targetsize(mosdepth_bed):
    name = mosdepth_bed.split('.')[0]
    df = pd.read_csv(mosdepth_bed, sep='\t', header=None)
    df.columns = 'chrom start end cov'.split()
    df['size'] = df['end'] - df['start']
    x = list(df['size'].values)
    y = list(df['cov'].values)
    plt.scatter(x, y)
    plt.xlabel('TargetSize')
    plt.ylabel('MosDepth')
    plt.title(name)
    plt.savefig(f'{name}_covlength.png', dpi=120)
    plt.close()
    return


def plot_coverage(mosdepth_bed):
    name = mosdepth_bed.split('.')[0]
    df = pd.read_csv(mosdepth_bed, sep='\t', header=None)
    df.columns = 'chrom start end cov'.split()
    plt.plot(list(df['cov'].values), '*')
    plt.xlabel('Target')
    plt.ylabel('Depth')
    plt.title(name)
    plt.savefig(f'{name}_cov.png', dpi=120)
    plt.close()
    return


def main(mosdepth_bed):
    plot_coverage_vs_targetsize(mosdepth_bed)
    plot_coverage(mosdepth_bed)
    return


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-b", "--bed", 
        type=str, 
        required=True, 
        help="Mosdepth output bed file"
        )
    args = parser.parse_args()                          
    main(args.bed)


import sqlite3

class CheckGenesisExport:
    def __init__(self, fluidx):
        DB = '/data/dnadiag/databases/HeadHoncho.db'
        self.conn = sqlite3.connect(DB)
        self.c = self.conn.cursor()
        self.fluidx = fluidx

    def get_sampleIDs_for_fluidx(self):
        samples = list()
        self.c.execute(f"SELECT SAMPLE FROM EXCELTOOLS WHERE FLUIDX = '{self.fluidx}'")
        for sample in self.c.fetchall():
            samples.extend(sample)
        return samples

    def check_if_sample_in_genesis(self, sampleID):
        self.c.execute(f"SELECT COUNT(*) FROM GENESIS WHERE SAMPLE = '{sampleID}'")
        return self.c.fetchone()[0]
            
    def check_if_all_samples_in_genesis(self):
        samples = self.get_sampleIDs_for_fluidx()
        for sample in samples:
            if self.check_if_sample_in_genesis(sample) == 0:
                print(sample)
        return


if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--fluidx", type=str, help="FluidX ID", required=True)
    args = parser.parse_args()

    fluidx = args.fluidx

    CGE = CheckGenesisExport(fluidx)
    CGE.check_if_all_samples_in_genesis()
import pysam


def get_probe_list(bedfile):
    probes = list()
    with open(bedfile) as f:
        for line in f:
            chrom, start, stop, strand, probe = line.split()
            start = int(start) -9 # BED start coordinate, 10 extra bases
            stop = int(stop)
            while start <= stop + 10:
                probes.append(f'{chrom}:{start}')
                start += 1
    return probes


def get_clusters_of_reads(bamfile, outputfile, cutoff=None):

    if cutoff is None: 
        cutoff = 5

    samfile = pysam.AlignmentFile(bamfile, "rb")
    clustercount = dict()
    total = 0

    for alignment in samfile:
        if alignment.is_unmapped:
            continue
        start = f'{alignment.reference_name}:{alignment.reference_start}'
        end = f'{alignment.reference_name}:{alignment.reference_end}'
        if start in clustercount:
            clustercount[start] += 1
        elif not start in clustercount:
            clustercount[start] = 0
        if end in clustercount:
            clustercount[end] += 1
        elif not end in clustercount:
            clustercount[end] = 0
            
    clusters_above_cutoff = dict()
    for cluster, count in clustercount.items():
        if count > cutoff:
            clusters_above_cutoff[cluster] = count
    return clusters_above_cutoff


def exclude_ontarget_clusters(bedfile, clustercount):
    clustercount_offtarget = dict()
    probes = get_probe_list(bedfile)
    for cluster, count in clustercount.items():
        if not cluster in probes:
            clustercount_offtarget[cluster] = count
            
    return clustercount_offtarget
    
    
def main(bamfile, bedfile, outputfile, cutoff):
    clusters_above_cutoff = get_clusters_of_reads(bamfile, outputfile, cutoff)
    clustercount_offtarget = exclude_ontarget_clusters(bedfile, clusters_above_cutoff)
    with open(outputfile, 'w') as f:
        for cluster, count in clustercount_offtarget.items():
            f.write(f'{cluster}\t{count}\n')
    
        
if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--bam", type=str, 
                        help="Input bamfile", required=True)
    parser.add_argument("--bed", type=str, 
                        help="BED file with targets to exclude", required=True)
    parser.add_argument("-o", "--output", type=str, 
                        help="Output file", required=True)                        
    parser.add_argument("-c", "--cutoff", type=str, 
                        help="Cutoff voor een cluster")                        
                        
                        
    args = parser.parse_args()
    
    bamfile = args.bam
    bedfile = args.bed
    outputfile = args.output
    cutoff = args.cutoff
    
    main(bamfile, bedfile, outputfile, cutoff)






    
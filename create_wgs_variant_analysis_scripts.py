import argparse

DORADO_MEM = '156G'
DORADO_THREADS = 96

MOSDEPTH_MEM = '8G'
MOSDEPTH_THREADS = 2

CLAIR3_MEM = '128G'
CLAIR3_THREADS = 48

SNIFFLES_MEM = '16G'
SNIFFLES_THREADS = 4

SCRATCH = '/net/beegfs/scratch/mhaagmans'

T2TFA = f'{SCRATCH}/reference/T2T/chm13v2.0.fa'
HG38FA = '?'

MOSDEPTH = f'{SCRATCH}/programs/mosdepth'
CLAIR3_MODEL = f'{SCRATCH}/models/rerio/clair3_models/r1041_e82_400bps_sup_v500'


def write_sbatch_script(sampleID, toolID, content):
    with open(f'{sampleID}_{toolID}.sh', 'w') as f:
        for line in content.split('\n'):
            f.write(f'{line.lstrip()}\n')
    return


def main(sample, bam, ref_requested, align):
    if ref_requested == 'hg38':
        REFFA = HG38FA
    elif ref_requested == 'T2T':
        REFFA = T2TFA
        
    DORADO_OUT = f'{sample}.{ref_requested}.bam'
    MOSDEPTH_OUT = f'{sample}.{ref_requested}.mosdepth'
    CLAIR3_OUTPUT = f'{sample}.{ref_requested}.clair3'
    SNIFFLES_OUT = f'{sample}.{ref_requested}.snf'

    dorado_cmd = f'''#!/bin/sh
        #SBATCH --mem={DORADO_MEM}
        #SBATCH --cpus-per-task={DORADO_THREADS}
        #SBATCH -J {sample}_align
        #SBATCH --output=SL-%x.%j.out
        module load samtools
        module load dorado/0.8.1
        dorado aligner \\
        --threads {DORADO_THREADS} \\
        {REFFA} \\
        {bam} |\\
        samtools sort -@{DORADO_THREADS} - -o {DORADO_OUT} \\
        && samtools index -@{DORADO_THREADS} {DORADO_OUT}\n
        '''

    mosdepth_cmd = f'''#!/bin/sh
        #SBATCH --mem={MOSDEPTH_MEM}
        #SBATCH --cpus-per-task={MOSDEPTH_THREADS}
        #SBATCH -J {sample}_mosdepth
        #SBATCH --output=SL-%x.%j.out
        {MOSDEPTH} --no-per-base \\
        --threads {MOSDEPTH_THREADS} \\
        {MOSDEPTH_OUT} \\
        {DORADO_OUT}
        '''
        
    clair_cmd = f'''#!/bin/sh
        #SBATCH --mem={CLAIR3_MEM}
        #SBATCH --cpus-per-task={CLAIR3_THREADS}
        #SBATCH -J {sample}_clair3
        #SBATCH --output=SL-%x.%j.out
        module load conda
        conda activate clair3
        run_clair3.sh \\
        --bam_fn={DORADO_OUT} \\
        --output={CLAIR3_OUTPUT} \\
        --ref_fn={REFFA} \\
        --model_path={CLAIR3_MODEL} \\
        --threads={CLAIR3_THREADS} \\
        --platform=ont \\
        --use_whatshap_for_final_output_phasing \\
        --use_whatshap_for_final_output_haplotagging         
         '''
    
    sniffles_cmd = f'''#!/bin/sh
        #SBATCH --mem={SNIFFLES_MEM}
        #SBATCH --cpus-per-task={SNIFFLES_THREADS}
        #SBATCH -J {sample}_sniffles2
        #SBATCH --output=SL-%x.%j.out
        module load conda
        conda activate sniffles2
        sniffles \\
        --input {DORADO_OUT} \\
        --snf {SNIFFLES_OUT}
         '''        
         
    if align:
        write_sbatch_script(sample, 'dorado', dorado_cmd)
        
    write_sbatch_script(sample, 'mosdepth', mosdepth_cmd)
    write_sbatch_script(sample, 'clair3', clair_cmd)
    write_sbatch_script(sample, 'sniffles2', sniffles_cmd)
        
    if align:
        with open(f'{sample}_submitter.sh', 'w') as f:
            f.write(f'JOBID=$(sbatch --parsable {sample}_dorado.sh)\n')
            f.write(f'sbatch --dependency=afterok:${{JOBID}} {sample}_mosdepth.sh\n')
            f.write(f'sbatch --dependency=afterok:${{JOBID}} {sample}_clair3.sh\n')
            f.write(f'sbatch --dependency=afterok:${{JOBID}} {sample}_sniffles2.sh\n')
            
    elif not align:
        with open(f'{sample}_submitter.sh', 'w') as f:
            f.write(f'sbatch {sample}_mosdepth.sh\n')
            f.write(f'sbatch {sample}_clair3.sh\n')
            f.write(f'sbatch {sample}_sniffles2.sh\n')

    return


if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--sample", type=str, help="SampleID", required=True)
    parser.add_argument("-b", "--bam", type=str, help="Path to BAM", required=True)
    parser.add_argument("-g", "--genome", type=str, help="hg38 or T2T; default=T2T", default='T2T')
    parser.add_argument("--noalign", action='store_true')    

    args = parser.parse_args()
    sample = args.sample
    bam = args.bam
    ref_requested = args.genome
    no_align = args.noalign
    
    if no_align:
        align = False
    else:
        align = True
    
    main(sample, bam, ref_requested, align)
    
    
    





















    
    
    
import argparse
import sqlite3
import pandas as pd


class GetInfo:
    def __init__(self, database, serie):
        self.serie = serie
        self.conn = sqlite3.connect(database)
        self.c = self.conn.cursor()

    def __del__(self):
        self.conn.close()

    def _get_tables(self):
        self.c.execute("SELECT name FROM sqlite_master WHERE type='table'")
        tables = [val for tup in self.c.fetchall() for val in tup]
        return tables
    
    def getinfo(self):
        d = dict()
        for table in self._get_tables():
            if table in ['todo', 'snpcheck', 'riskscore']:
                continue
            dflist = list()
            for serie in self.serie:
                sql = f"""SELECT * FROM {table}
                    WHERE SERIE='{serie}'
                    """
                _df = pd.read_sql(sql, con=self.conn)
                dflist.append(_df)
            d[table] = pd.concat(dflist)
        return d
    
    def write_excel(self, metrics):
        with pd.ExcelWriter(f'{self.serie}.xlsx', engine='xlsxwriter') as wb: 
            for table, df in metrics.items():
                df.to_excel(wb, sheet_name=table)
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Delete serie from database")
    parser.add_argument('-db', '--database', type=str, required=True,
                        help='Database to query')
    parser.add_argument('-se', '--serie', type=str, required=True,
                        help='Serie(s) to write)')

    args = parser.parse_args()
    database = args.database
    serie = args.serie
    if ',' in serie:
        serie = serie.split(',')
    else:
        serie = [serie]
    print(serie)
    GI = GetInfo(database, serie)
    info = GI.getinfo()
    # print(info)
    GI.write_excel(info)
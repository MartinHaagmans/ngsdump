import csv
import sqlite3
import logging
from pathlib import Path
from datetime import date

logging.basicConfig(
    format='%(asctime)s - %(levelname)s - %(message)s', 
    filename='whattodo.log',
    level=logging.INFO
    )


DB = '/data/dnadiag/databases/HeadHoncho.db'

def make_file_utf8(sourceFileName, targetFileName):
    import codecs
    BLOCKSIZE = 1048576 # or some other, desired size in bytes
    with codecs.open(sourceFileName, "r", "iso-8859-1") as sourceFile:
        with codecs.open(targetFileName, "w", "utf-8") as targetFile:
            while True:
                contents = sourceFile.read(BLOCKSIZE)
                if not contents:
                    break
                targetFile.write(contents)
    return


class CreateHeadHoncho:

    def __init__(self, db):    
        conn = sqlite3.connect(db)
        self.c = conn.cursor()
        self.conn = conn

    def create_tables(self):
        exceltools_create_sql = '''
            CREATE TABLE "EXCELTOOLS" (
                "FLUIDX" TEXT NOT NULL,
                "SAMPLE" TEXT NOT NULL,
                "HYBSERIE" TEXT NOT NULL,
                "SEX" TEXT,
                "AANVRAAGNUMMER" TEXT,
                "DOB" TEXT,
                "DEADLINE" TEXT,
                "ISOLATION" TEXT NOT NULL,
                PRIMARY KEY("FLUIDX","SAMPLE")
            )
        '''

        genesis_create_sql = '''
            CREATE TABLE "GENESIS" (
            "SAMPLE"	TEXT NOT NULL,
            "GENESIS"	TEXT NOT NULL,
            "DATE"	TEXT NOT NULL,
            "EXPORT"	TEXT NOT NULL,
            "ISOLATION"	TEXT NOT NULL,
            PRIMARY KEY("SAMPLE","GENESIS","DATE")
            )
        '''
        for sql in [exceltools_create_sql, genesis_create_sql]:
            try:
                self.c.execute(sql)
            except sqlite3.OperationalError:
                logging.warning(f"ᕦ(ò_óˇ)ᕤ Table already exists!")
        return

    def close(self):
        self.conn.close()
        return


class ExcelToolsDB:

    def __init__(self, db):
        conn = sqlite3.connect(db)
        self.c = conn.cursor()
        self.conn = conn

    def get_fluidX_barcode(self, fn):
        fluidX = str(fn).split('_')[-1]
        fluidX = fluidX.replace('.csv', '')
        return fluidX
    
    def parse_amplicon_line(line):
        if len(line) == 3:
            _, hybserie, dnr = line
            target = 'GNAS-MAS'
        elif len(line) == 4:
            _, hybserie, dnr, target = line
        else:
            logging.warning(f"¿ⓧ_ⓧﮌ {line} can't be parsed -> Skipped.")
            return
        return (hybserie, dnr, target)

    def parse_excel_tools_export(self, fn):
        overview = dict()
        with open(fn) as f:
            _header = next(f)
            reader = csv.reader(f)    
            for line in reader:
                if line[0].lower().startswith('amplicon'):
                    continue
                else:    
                    hybserie, dnr, isolation_method, sex, danr, dob, deadline = line
                overview[dnr] = {
                    'HYBserie': hybserie,
                    'Isolation_method': isolation_method,
                    'Sex': sex,
                    'Aanvraagnummer': danr,
                    'DOB': dob,
                    'Deadline': deadline,
                }
        return overview
    
    def parse_genesis_export(self, fn):
        export_date = Path(fn).name.replace('_ngsexport.csv', '')
        export_date = export_date.replace('.utf8.csv', '')
        genesis_export = dict()
        with open(fn) as f:
            reader = csv.reader(f, delimiter='\t')
            header = next(reader)
            column = {header_name: i for i, header_name in enumerate(header)}
            for line in reader:
                sampleID = line[column['FractionNr']]
                genesis_code = line[column['VerrichtingName']]
                isolation = line[column['ExtractionMethod']]
                if isolation == '':
                    isolation = 0
                if genesis_code.endswith('NGS') and sampleID not in genesis_export:
                    genesis_export[sampleID] = dict()
                    genesis_export[sampleID]['ExportDatum'] = export_date
                    genesis_export[sampleID]['Verrichtingen'] = list()
                    genesis_export[sampleID]['Verrichtingen'].append(genesis_code)
                    genesis_export[sampleID]['Isolation'] = isolation
                elif genesis_code.endswith('NGS') and sampleID in genesis_export:
                    genesis_export[sampleID]['Verrichtingen'].append(genesis_code)

        for sample in genesis_export.keys():
            if len(genesis_export[sample]['Verrichtingen']) == 1:
                genesis = genesis_export[sample]['Verrichtingen'][0].split('-')[0]
                genesis_export[sample]['Genesis'] = genesis
            elif len(genesis_export[sample]['Verrichtingen']) > 1:
                genesis = [_.split('-')[0] for _ in genesis_export[sample]['Verrichtingen']]
                assert len(set(genesis)) == 1, f"{genesis_export[sample]['Verrichtingen']} not same capture"
                genesis_export[sample]['Genesis'] = genesis[0]
        return genesis_export

    def add_exceltools_to_db(self, samples, fluidx):
        for sample, sample_info in samples.items():
            hybserie = sample_info['HYBserie']
            isolation_method = sample_info['Isolation_method']
            sex = sample_info['Sex']
            danr = sample_info['Aanvraagnummer']
            dob = sample_info['DOB']
            deadline = sample_info['Deadline']

            sql_add = f'''
            INSERT INTO EXCELTOOLS
            (FLUIDX, SAMPLE, HYBSERIE, SEX, 
            AANVRAAGNUMMER, DOB, DEADLINE, ISOLATION)
            VALUES("{fluidx}", "{sample}", "{hybserie}", "{sex}",
            "{danr}", "{dob}", "{deadline}", "{isolation_method}"
            ) ;
            '''
            try:
                self.c.execute(sql_add)
            except sqlite3.IntegrityError:
                logging.warning(f"(⊙_◎) {sample} for {fluidx} already in exceltools database!")
            else:
                self.conn.commit()
        return

    def add_genesis_to_db(self, samples):
        for sample, sample_info in samples.items():
            genesis = sample_info['Genesis']
            isolation_method = sample_info['Isolation']
            verrichtingen = sample_info['Verrichtingen']
            export_date = sample_info['ExportDatum']

            sql_add = f'''
            INSERT INTO GENESIS
            (SAMPLE, GENESIS, DATE,
            EXPORT, ISOLATION)
            VALUES("{sample}", "{genesis}", 
            "{export_date}", "{verrichtingen}", 
            "{isolation_method}"
            ) ;
            '''
            try:
                self.c.execute(sql_add)
            except sqlite3.IntegrityError:
                logging.warning(f"(⊙_◎) {sample} already in genesis database!")
            else:
                self.conn.commit()
        return

    def move_file_if_done(self, fn):
        done_folder = Path(fn).parent / 'done'
        fn.rename(done_folder / fn.name)
        return
    
    def close(self):
        self.conn.close()
        return    
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
        description="Parse ExcelTools and GenesisExport"
        )
    
    parser.add_argument(
        '-e', '--exceltools', 
        type=str, 
        help='ExcelTools export to parse',
        )
    
    parser.add_argument(
        '-g', '--genesis', 
        type=str,
        help='Genesis export to parse',
        )

    parser.add_argument(
        '--create_database', 
        action='store_true',
        help='Create Head Honcho',
        )     

    args = parser.parse_args()
    exportExcelTools = args.exceltools
    exportGenesis = args.genesis

    if args.create_database:
        CHH = CreateHeadHoncho(DB)
        CHH.create_tables()
        CHH.close()
        exit()

    ETDB = ExcelToolsDB(DB)

    if exportExcelTools:
        fluidx = ETDB.get_fluidX_barcode(exportExcelTools)
        overview = ETDB.parse_excel_tools_export(exportExcelTools)
        ETDB.add_exceltools_to_db(overview, fluidx)
        ETDB.move_file_if_done(Path(exportExcelTools))
        logging.info(f'ʕ•ᴥ•ʔ Done for {fluidx}')

    if exportGenesis:
        try:
            genesis = ETDB.parse_genesis_export(exportGenesis)
        except UnicodeDecodeError as e:
            make_file_utf8(exportGenesis, f'{exportGenesis}.utf8.csv')
            genesis = ETDB.parse_genesis_export(f'{exportGenesis}.utf8.csv')
        ETDB.add_genesis_to_db(genesis)
        try:
            Path(f'{exportGenesis}.utf8.csv').unlink()
        except FileNotFoundError:
            logging.info(f'ᵒᴥᵒ# No utf8 to remove {exportGenesis}')    

        ETDB.move_file_if_done(Path(exportGenesis))
        logging.info(f'ʕ•ᴥ•ʔ Done for {exportGenesis}')
    
    ETDB.close()







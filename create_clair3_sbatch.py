import argparse

CLAIR3_MEM = '128G'
CLAIR3_THREADS = 48

SCRATCH = '/net/beegfs/scratch/mhaagmans'

T2T = f'{SCRATCH}/reference/T2T/chm13v2.0.fa'
HG38 = f'{SCRATCH}/reference/hg38_UCSC/hg38_custom.fa'

MOSDEPTH = f'{SCRATCH}/programs/mosdepth'



def write_sbatch_script(sampleID, toolID, content):
    with open(f'{sampleID}_{toolID}.sh', 'w') as f:
        for line in content.split('\n'):
            f.write(f'{line.lstrip()}\n')
    return


def main(sample, bam, ref_requested, model):
    
    CLAIR3_OUTPUT = f'{sample}.{ref_requested}.{model}.clair3'
    
    if ref_requested == 'hg38':
        REFFA = HG38
    elif ref_requested == 'T2T':
        REFFA = T2T
        
    if model == 'v5':
        CLAIR3_MODEL = f'{SCRATCH}/models/rerio/clair3_models/r1041_e82_400bps_sup_v500'
    elif model == 'v4':
        CLAIR3_MODEL = f'{SCRATCH}/models/rerio/clair3_models/r1041_e82_400bps_sup_v430'

    clair_cmd = f'''#!/bin/sh
        #SBATCH --mem={CLAIR3_MEM}
        #SBATCH --cpus-per-task={CLAIR3_THREADS}
        #SBATCH -J {sample}_clair3
        #SBATCH --output=SL-%x.%j.out
        source $HOME/.bashrc
        conda activate clair3
        run_clair3.sh \\
        --bam_fn={bam} \\
        --output={CLAIR3_OUTPUT} \\
        --ref_fn={REFFA} \\
        --model_path={CLAIR3_MODEL} \\
        --threads={CLAIR3_THREADS} \\
        --platform=ont \\
        --use_whatshap_for_final_output_phasing \\
        --use_whatshap_for_final_output_haplotagging         
        '''
    
    write_sbatch_script(sample, 'clair3', clair_cmd)
        
    return


if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--sample", type=str, help="SampleID", required=True)
    parser.add_argument("-b", "--bam", type=str, help="Path to BAM", required=True)
    parser.add_argument("-g", "--genome", type=str, help="hg38 or T2T; default=T2T", default='hg38')
    parser.add_argument("-m", "--model", type=str, help="Basecalling model: v4 or v5", default='v5')

    args = parser.parse_args()
    sample = args.sample
    bam = args.bam
    ref_requested = args.genome
    basecalling_model = args.model
    
    
    main(sample, bam, ref_requested, basecalling_model)    
    
    
    





















    
    
    
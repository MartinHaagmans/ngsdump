import argparse
import subprocess

CALCULATE_METH_SCRIPT = "/media/sf_Documents/GitHub/ontmethylationpipeline/ontmethylation/calculate_methylation_frequency.py"

def run_command(command):
    subprocess.run(
        command, shell=True, check=True,
        stderr=subprocess.STDOUT
        )
    return

def parse_targetbed_to_intervals(target_bed):
    intervals = list()
    with open(target_bed) as f:
        for line in f:
            chromosome, start, end, *_ = line.split()
            #BED file is zero based coordinate system
            start = int(start) + 1
            intervals.append(f'{chromosome}:{start}-{end}')
    return intervals
    
def call_methylation(ref, sample, intervals):
    for interval in intervals:
        interval_name = interval.replace(':', '-')
        interval_name = interval_name.replace('-', '_')
        
        interval_output = f'{sample}_{interval_name}_methylation_calls.txt'
        
        run_command(f'nanopolish call-methylation -r {sample}.fastq -b {sample}.sorted.bam -g {ref} '
              f'-w "{interval}" > {interval_output}')
    
    
def calculate_methylation(sample, intervals):
    for interval in intervals:
        interval_name = interval.replace(':', '-')
        interval_name = interval_name.replace('-', '_')     

        interval_input = f'{sample}_{interval_name}_methylation_calls.txt'
        interval_output = f'{sample}_{interval_name}_methylation_freq.txt'
        run_command(f'python3 {CALCULATE_METH_SCRIPT} {interval_input} > {interval_output}')
                

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Call methylation from BAM for BED targets'
        )
    
    parser.add_argument('-s', '--sample', type=str, required=True,
        help='Sample name for output files')
    parser.add_argument('-r', '--ref', type=str, required=True,
        help='Reference fasta')
    parser.add_argument('--target', type=str, required=True,
        help='BED file with target regions')        
    args = parser.parse_args()

    sample = args.sample
    reference = args.ref
    target_bed = args.target
    intervals = parse_targetbed_to_intervals(target_bed)
    call_methylation(reference, sample, intervals)
    calculate_methylation(sample, intervals)

        
        
        
        
        
        
        
        
        

def make_comp(base):
    if base == 'A':
        return 'T'
    elif base == 'T':
        return 'A'
    elif base == 'C':
        return 'G'
    elif base == 'G':
        return 'C'    


def reverse_complement(seq):
    #Reverse seq
    seq = seq[::-1]
    rev_comp = list()
    for base in seq:
        comp_base = make_comp(base)
        rev_comp.append(comp_base)
    return ''.join(rev_comp)
        
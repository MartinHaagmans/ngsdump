import csv

def parse_report(fn):

    counts = dict()
    
    with open(fn) as f:
        header = next(f)
        reader = csv.reader(f)
        for line in reader:
            call = line[-1]
            if call in counts:
                counts[call] +=1
            elif not call in counts:
                counts[call] = 1
    return counts
    
    
def make_counts_percentages(counts):
        counts_perc = dict()
        
        total = 0
        
        for call, count in counts.items():
            total += count
            
        for call, count in counts.items():
            counts_perc[call] =  count / total * 100
            
        return counts_perc
        


                
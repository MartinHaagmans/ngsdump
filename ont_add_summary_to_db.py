import sys
import yaml
import sqlite3


EXPTYPES = ['CAS9', 'AS']

def check_args(args):
    if not args.date.startswith('20') or len(args.date.split('-')) != 3:
        raise ValueError('Date shoud be YYYY-MM-DD')

    if not args.exptype in EXPTYPES:
        raise ValueError('Experiment type not available. Available types are: {}'.format(', '.join(EXPTYPES)))

def parse_summary(summary):
    with open(summary) as f:
        summary = yaml.load(f, Loader=yaml.FullLoader)

    all_reads = summary['All reads']
    qual_reads = summary['Q>=7']    

    # maybe add this later
    del all_reads['reads']
    del all_reads['gigabases']
    return all_reads


def parse_info_for_database(fn, sample, ng_input, project, rundate, exptype, pores):
    info = parse_summary(fn)
    to_db = dict()
    to_db['sample'] = sample
    to_db['ng_input'] = ng_input
    to_db['project'] = project
    to_db['rundate'] = rundate
    to_db['exptype'] = exptype
    to_db['pores'] = pores
    to_db['totalreads'] = info['total.reads']
    to_db['totalgigabases'] = info['total.gigabases']
    to_db['N50'] = info['N50.length']
    to_db['meanlength'] = info['mean.length']
    to_db['medianlength'] = info['median.length']
    to_db['maxlength'] = info['max.length']
    to_db['meanq'] = info['mean.q']
    to_db['medianq'] = info['median.q']
    return to_db

    
def construct_sql_statement(to_db):
    columns = to_db.keys()
    columns = ', '.join(columns)    
    values = [str(value[1]) for value in to_db.items()]
    values = '\', \''.join(values)
    sql = '''INSERT INTO runmetrics 
    ({}) 
    VALUES ('{}')'''.format(columns, values)
    return sql

    
def add_to_database(sql, db):
    conn = sqlite3.connect(db)
    c = conn.cursor()
    c.execute(sql)
    conn.commit()
    conn.close()
    return
    
    
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description="Add stuff to db")
    parser.add_argument('--database', type=str, required=True,
                        help='Database location')    
    parser.add_argument('--summary', type=str, required=True,
                        help='MinIONQC\'s summary.yaml location')                            
    parser.add_argument('--sample', type=str, required=True,
                        help='SampleID as in folder QC')
    parser.add_argument('--date', type=str, required=True,
                        help="Date run as in folder QC")
    parser.add_argument('--project', type=str, required=True,
                        help='Project name as in folder QC (CLOCK, NINJ etc.)')
    parser.add_argument('--exptype', type=str, required=True,
                        help='Type of experiment (CAS9 enrichment, Adaptive sampling etc.)')
    parser.add_argument('--ng_input', type=str,
                        help='ng loaded at flowcell')                        
    parser.add_argument('--pores', type=str,
                        help='Pores active at start run')                        

    args = parser.parse_args()
    
    try:
        check_args(args)
    except ValueError as e:
        print(e)
        sys.exit()
    
    info = parse_info_for_database(
        args.summary, 
        args.sample, 
        args.ng_input, 
        args.project, 
        args.date, 
        args.exptype, 
        args.pores
        )
        
    sql = construct_sql_statement(info)
    try:
        add_to_database(sql, args.database)
    except sqlite3.IntegrityError:
        print('ALREADY IN DATABASE:\nSample: {}\nProject: {}\nDate: {}'.format(args.sample, args.project, args.date))

#!/usr/bin/env python
# coding: utf-8
import csv
import glob
import pandas as pd

class SNPnotParsed(Exception):
    pass

class SNPparser:

    def __init__(self, clair3_fn, mosdepth_fn, ref_fn=None):
        if ref_fn is None:
            self.ref_fn = "SNP_ref.txt"
        self.HET = '0|1 1|0 0/1'.split()
        self.HOM = '1|1 1/1'.split()
        self.clair3 = clair3_fn
        self.mosdepth = mosdepth_fn
        
    def create_refbase_dict(self, ref_fn):
        "Parse SNP.txt containing chr:pos refbase from samtools faidx output. Return dict"
        refbase_dict = dict()
        with open(ref_fn) as f:
            reader = csv.reader(f)
            for line in reader:
                _interval, base = line
                locus = _interval.split('-')[0]
                refbase_dict[locus] = base
        return refbase_dict

    def parse_clair3_intersect(self, clair3_fn):
        "Parse clair3 bedtools intersect. Return dict."
        clair3_dict = dict()
        with open(clair3_fn) as f:
            for line in f:
                chrom, pos, _id, ref, alt, _qual, filt, _origin, gt_info, gt_metrics = line.split()
                locus = f"{chrom}:{pos}"
                genotype = gt_metrics.split(':')[0]
                cov = gt_metrics.split(':')[2]
                if genotype in self.HOM:
                    genotype_call = 'HOM'
                    genotype_bases = f'{alt}/{alt}'
                elif not genotype in self.HOM:
                    genotype_call = 'HET'
                    genotype_bases = f'{ref}/{alt}'
                clair3_dict[locus] = (genotype_bases, genotype_call, cov)
        return clair3_dict

    def parse_mosdepth_intersect(self, mosdepth_fn):
        "Parse mosdepth bedtools intersect. Return dict."
        mosdepth_dict = dict()
        with open(mosdepth_fn) as f:
            for line in f:
                chrom, _pos, pos, cov, *_ = line.split()
                locus = f'{chrom}:{pos}'
                mosdepth_dict[locus] = cov
        return mosdepth_dict

    def create_output(self):
        "Combine clair3 and mosdepth into calls per locus. Return dict."
        output = dict()
        
        refbases = self.create_refbase_dict(self.ref_fn)
        clair3 = self.parse_clair3_intersect(self.clair3)
        mosdepth = self.parse_mosdepth_intersect(self.mosdepth)
        
        for snp, base in refbases.items():
            if snp in clair3:
                output[snp] = clair3[snp]
            elif not snp in clair3 and snp in mosdepth:
                output[snp] = (f'{base}/{base}', 'WT', mosdepth[snp])
            else:
                raise SNPnotParsed(f'SNP {snp} should be in clair3 or mosdepth output')
        return output
        
    def write_output_to_file(self, output_fn):
        output = self.create_output()
        with open(output_fn, 'w') as f:
            for locus, data in output.items():
                gt, call, dp = data
                f.write(f'{locus}\t{gt}\t{call}\t{dp}\n')
        return
        
        


if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--clair3", type=str, 
                        help="Clair3 VCF intersected with targetBED", required=True)
    parser.add_argument("-m", "--mosdepth", type=str, 
                        help="Mosdepth output intersected with targetBED", required=True)
    parser.add_argument("-s", "--sample", type=str, 
                        help="SampleID to name output")

    args = parser.parse_args()
    
    clair3_fn = args.clair3
    mosdepth_fn = args.mosdepth
    sample = args.sample
    
    if not sample:
        sample = 'SampleID'
        
    S = SNPparser(
        clair3_fn,
        mosdepth_fn
    ).write_output_to_file(f'{samnple}_SNP_combined.txt')
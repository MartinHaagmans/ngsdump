#!/bin/sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SCRIPTS=/data/dnadiag/ngsdump

python3 ${SCRIPTS}/transfer_aumc_exports.py


for fn_e in $DIR/exceltools/*.csv ; do
	if [[ -e "${fn_e}" ]] ; then
		python3 ${SCRIPTS}/parse_aumc_export.py --exceltools $fn_e ;
	fi ;
done ;

for fn_g in $DIR/genesis/*.csv ; do
	if [[ -e "${fn_g}" ]] ; then
        	python3 ${SCRIPTS}/parse_aumc_export.py --genesis $fn_g ;
	fi ;
done ;

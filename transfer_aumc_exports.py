import shutil
import logging

from pathlib import Path

logging.basicConfig(
    format='%(asctime)s - %(levelname)s - %(message)s', 
    filename='whattodo.log',
    level=logging.INFO
    )

indir_genesis = Path('/mnt/TargetedPanels/genesis_export_files/')
indir_exceltools = Path('/mnt/TargetedPanels/Targeted Panels/CSVfiles/')
to_parse_genesis_folder = Path('/data/dnadiag/todolists/genesis')
to_parse_exceltools_folder = Path('/data/dnadiag/todolists/exceltools')
parsed_genesis_folder = to_parse_genesis_folder / 'done'
parsed_exceltools_folder = to_parse_exceltools_folder / 'done'

parsed_genesis_fns = [fn.name for fn in parsed_genesis_folder.iterdir()]
parsed_exceltools_fns = [fn.name for fn in parsed_exceltools_folder.iterdir()]

doneSomething = False

for fn in indir_genesis.glob('*/*.CSV'):
    if fn.is_file():
        delivery_date = str(fn).split('/')[-2]
        try:
            int(delivery_date)
        except ValueError:
            logging.error(f"¯\_(⊙︿⊙)_/¯ Directory with date not as should be: {delivery_date}")
            exit()
        finally:
            parsed_already = f'{delivery_date}_ngsexport.csv' in parsed_genesis_fns
        if not parsed_already:
            shutil.copy(fn, to_parse_genesis_folder / f'{delivery_date}_ngsexport.csv')
            logging.info(f"ʕ•ᴥ•ʔ Transfer done for {delivery_date}_ngsexport.csv")
            doneSomething = True

for fn in indir_exceltools.glob('*.csv'):
    if fn.is_file() and 'NGS' in str(fn) and not fn.name in parsed_exceltools_fns:
        shutil.copy(fn, to_parse_exceltools_folder)
        logging.info(f"ʕ•ᴥ•ʔ Transfer done for {fn.name}")
        doneSomething = True

if not doneSomething:
    logging.info("ʕ •`ᴥ•´ʔ I woke up for this?")
elif doneSomething:
    logging.info("ʕᵔᴥᵔʔ I did something!")
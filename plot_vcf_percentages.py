import matplotlib.pyplot as plt
from vcf_parser import VCFParser
from statistics import mean,stdev


import matplotlib.pyplot as plt
from vcf_parser import VCFParser
from statistics import mean,stdev


class VariantPercentages:

    def __init__(self, vcf_fn, sampleID):
        self.vcf_fn = vcf_fn
        self.sampleID = sampleID
        self.homperc, self.hetperc = self._varpercentages_from_vcf()
        self.varpercentages = self.homperc + self.hetperc
    

    def _variant_is_snp(self, var):
        ref_length = len(var['REF']) == 1
        var_length = len(var['ALT']) == 1
        is_snp = ref_length and var_length 
        return is_snp
        

    def _varpercentages_from_vcf(self):
        
        hetperc = list()
        homperc = list()

        vcfreader = VCFParser(
            infile=self.vcf_fn, 
            split_variants=True, 
            check_info=True
        )

        for variant in vcfreader:
            if not self._variant_is_snp(variant):
                continue

            gt, ad, *_ = variant[self.sampleID].split(':')
            refcount, varcount = ad.split(',')
            depth = int(refcount) + int(varcount)

            if not depth > 100:
                continue

            varpercentage = int(varcount) / depth

            if gt == '0/1':
                hetperc.append(varpercentage)
            elif gt == '1/1':
                homperc.append(varpercentage)
        
        return homperc, hetperc


    def plot_varpercentage(self, plot_fn):
        fig = plt.figure()
        ax = plt.subplot()
        ax.plot(self.varpercentages, 'd')
        ax.set_ylim(0, 1)
        ax.axhline(y=0.5)
        plt.title(self.sampleID)
        plt.savefig(plot_fn, dpi=80)
        plt.close()
        return
        
    def report(self):
        "Returns list of tuples"
        count_hom = len(self.homperc)
        count_het = len(self.hetperc)
        
        mean_hom = mean(self.homperc)
        mean_het = mean(self.hetperc)
        stdev_hom = stdev(self.homperc)
        stdev_het = stdev(self.hetperc)
        
        return [
            (mean_hom, stdev_hom), 
            (mean_het, stdev_het),
            (count_hom, count_het)
            ]
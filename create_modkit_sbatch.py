import argparse

MODKIT_MEM = '36G'
MODKIT_THREADS = 24

SCRATCH = '/net/beegfs/scratch/mhaagmans'

HG38FA = f'{SCRATCH}/reference/hg38_UCSC/hg38_custom.fa'
T2TFA = f'{SCRATCH}/reference/T2T/chm13v2.0.fa'

MODKIT = f'{SCRATCH}/programs/modkit_v0.4.1/modkit'


def write_sbatch_script(sampleID, toolID, content):
    with open(f'{sampleID}_{toolID}.sh', 'w') as f:
        for line in content.split('\n'):
            f.write(f'{line.lstrip()}\n')
    return


def main(sampleID, bam, ref_requested):
    if ref_requested == 'hg38':
        REFFA = HG38FA
    elif ref_requested == 'T2T':
        REFFA = T2TFA
        
    MODKIT_THRESH = f'{sampleID}.{ref_requested}.threshold.txt'
    MODKIT_SUMMARY = f'{sampleID}.{ref_requested}.modkit.summary.txt'
    MODKIT_OUT = f'{sampleID}.{ref_requested}.modkit.bed'
    MODKIT_TRAD_OUT = f'{sampleID}.{ref_requested}.modkit_traditional.bed'

    modkit_cmd_threshold = f'''#!/bin/sh
        #SBATCH --mem=8G
        #SBATCH --cpus-per-task=2
        #SBATCH -J {sampleID}_modkit_threshold
        #SBATCH --output=SL-%x.%j.out
        mkdir -p {sampleID}_probs
        {MODKIT} sample-probs \\
        --threads 2 --hist --out-dir {sampleID}_probs \\
        {bam} > {MODKIT_THRESH}\n
        '''
        
    modkit_cmd_summary = f'''#!/bin/sh
        #SBATCH --mem={MODKIT_MEM}
        #SBATCH --cpus-per-task={MODKIT_THREADS}
        #SBATCH -J {sampleID}_modkit_summary
        #SBATCH --output=SL-%x.%j.out    
        {MODKIT} summary --no-sampling \\
        --threads {MODKIT_THREADS} \\
        {bam} > {MODKIT_SUMMARY}     
        '''

    modkit_cmd_pileup = f'''#!/bin/sh
        #SBATCH --mem={MODKIT_MEM}
        #SBATCH --cpus-per-task={MODKIT_THREADS}
        #SBATCH -J {sampleID}_modkit_pileup
        #SBATCH --output=SL-%x.%j.out
        {MODKIT} pileup \\
        --threads {MODKIT_THREADS} \\
        {bam} {MODKIT_OUT} 
        '''

    modkit_cmd_pileup_trad = f'''#!/bin/sh
        #SBATCH --mem={MODKIT_MEM}
        #SBATCH --cpus-per-task={MODKIT_THREADS}
        #SBATCH -J {sampleID}_modkit_pileup_traditional
        #SBATCH --output=SL-%x.%j.out
        {MODKIT} pileup \\
        --threads {MODKIT_THREADS} --ref {REFFA} --preset traditional \\
        {bam} {MODKIT_TRAD_OUT} 
        '''
    write_sbatch_script(sampleID, 'modkit_threshold', modkit_cmd_threshold)
    write_sbatch_script(sampleID, 'modkit_summary', modkit_cmd_summary)
    write_sbatch_script(sampleID, 'modkit_pileup', modkit_cmd_pileup)
    write_sbatch_script(sampleID, 'modkit_pileup_traditional', modkit_cmd_pileup_trad)

    with open(f'{sampleID}_submitter.sh', 'w') as f:
        f.write(f'sbatch {sampleID}_modkit_threshold.sh\n')
        f.write(f'sbatch {sampleID}_modkit_pileup.sh\n')
        f.write(f'sbatch {sampleID}_modkit_pileup_traditional.sh\n')
        f.write(f'sbatch {sampleID}_modkit_summary.sh\n')
        
    return


if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--sampleID", 
        type=str, 
        help="SampleID", 
        required=True
        )
    parser.add_argument(
        "-b", "--bam", 
        type=str, 
        help="Path to BAM", 
        required=True
        )
    parser.add_argument(
        "-g", "--genome", 
        type=str, 
        help="hg38 or T2T; default=hg38", 
        default='hg38'
        )


    args = parser.parse_args()
    sampleID = args.sampleID
    bam = args.bam
    ref_requested = args.genome

    
    main(sampleID, bam, ref_requested)    
    
    
    





















    
    
    
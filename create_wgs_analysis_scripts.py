import argparse

DORADO_MEM = '40G'
DORADO_THREADS = 32

MODKIT_MEM = '10G'
MODKIT_THREADS = 16

MOSDEPTH_MEM = '3G'
MOSDEPTH_THREADS = 2

CLAIR3_MEM = '32G'
CLAIR3_THREADS = 24

SCRATCH = '/net/beegfs/scratch/mhaagmans'

HG38 = f'{SCRATCH}/reference/hg38_UCSC/hg38_custom.fa'
T2T = f'{SCRATCH}/reference/T2T/chm13v2.0.fa'

MODKIT = f'{SCRATCH}/programs/modkit_v0.4.3/modkit'
MOSDEPTH = f'{SCRATCH}/programs/mosdepth'
CLAIR3_MODEL = f'{SCRATCH}/models/rerio/clair3_models/r1041_e82_400bps_sup_v500'


def write_sbatch_script(sampleID, toolID, content):
    with open(f'{sampleID}_{toolID}.sh', 'w') as f:
        for line in content.split('\n'):
            f.write(f'{line.lstrip()}\n')
    return

def main(sample, bam, ref_requested, align):
    if ref_requested.upper() == 'HG38':
        REF = HG38
    elif ref_requested.upper() == 'T2T':
        REF = T2T
        
    DORADO_OUT = f'{sample}.{ref_requested}.bam'
    MODKIT_OUT = f'{sample}.{ref_requested}.modkit.bed'
    MODKIT_TRAD_OUT = f'{sample}.{ref_requested}.modkit_traditional.bed'
    MOSDEPTH_OUT = f'{sample}.{ref_requested}.mosdepth'
    CLAIR3_OUTPUT = f'{sample}.{ref_requested}.clair3'

    dorado_cmd = f'''#!/bin/sh
        #SBATCH --mem={DORADO_MEM}
        #SBATCH --cpus-per-task={DORADO_THREADS}
        #SBATCH -J {sample}_align
        #SBATCH --output=SL-%x.%j.out
        module load samtools
        module load dorado/0.9.0
        dorado aligner \\
        --threads {DORADO_THREADS} \\
        {REF} \\
        {bam} |\\
        samtools sort -@{DORADO_THREADS} - -o {DORADO_OUT} \\
        && samtools index -@{DORADO_THREADS} {DORADO_OUT}\n
        '''

    modkit_cmd_pileup = f'''#!/bin/sh
        #SBATCH --mem={MODKIT_MEM}
        #SBATCH --cpus-per-task={MODKIT_THREADS}
        #SBATCH -J {sample}_modkit_pileup
        #SBATCH --output=SL-%x.%j.out
        {MODKIT} pileup \\
        --threads {MODKIT_THREADS} \\
        {DORADO_OUT} {MODKIT_OUT} 
        '''

    modkit_cmd_pileup_trad = f'''#!/bin/sh
        #SBATCH --mem={MODKIT_MEM}
        #SBATCH --cpus-per-task={MODKIT_THREADS}
        #SBATCH -J {sample}_modkit_pileup_traditional
        #SBATCH --output=SL-%x.%j.out
        {MODKIT} pileup \\
        --threads {MODKIT_THREADS} --ref {REF} --preset traditional \\
        {DORADO_OUT} {MODKIT_TRAD_OUT} 
        '''

    mosdepth_cmd = f'''#!/bin/sh
        #SBATCH --mem={MOSDEPTH_MEM}
        #SBATCH --cpus-per-task={MOSDEPTH_THREADS}
        #SBATCH -J {sample}_mosdepth
        #SBATCH --output=SL-%x.%j.out
        {MOSDEPTH} \\
        --threads {MOSDEPTH_THREADS} \\
        --by 500 \\
        {MOSDEPTH_OUT} \\
        {DORADO_OUT}
        '''
        
    clair_cmd = f'''#!/bin/sh
        #SBATCH --mem={CLAIR3_MEM}
        #SBATCH --cpus-per-task={CLAIR3_THREADS}
        #SBATCH -J {sample}_clair3
        #SBATCH --output=SL-%x.%j.out
        source $HOME/.bashrc
        conda activate clair3        
        run_clair3.sh \\
        --bam_fn={DORADO_OUT} \\
        --output={CLAIR3_OUTPUT} \\
        --ref_fn={REF} \\
        --model_path={CLAIR3_MODEL} \\
        --threads={CLAIR3_THREADS} \\
        --platform=ont \\
        --use_whatshap_for_final_output_phasing \\
        --use_whatshap_for_final_output_haplotagging         
         '''    
         
    if align:
        write_sbatch_script(sample, 'dorado', dorado_cmd)
        
    write_sbatch_script(sample, 'modkit_pileup', modkit_cmd_pileup)
    write_sbatch_script(sample, 'modkit_pileup_traditional', modkit_cmd_pileup_trad)
    write_sbatch_script(sample, 'mosdepth', mosdepth_cmd)
    write_sbatch_script(sample, 'clair3', clair_cmd)    
        
    if align:
        with open(f'{sample}_submitter.sh', 'w') as f:
            f.write(f'JOBID=$(sbatch --parsable {sample}_dorado.sh)\n')
            f.write(f'sbatch --dependency=afterok:${{JOBID}} {sample}_modkit_pileup.sh\n')
            f.write(f'sbatch --dependency=afterok:${{JOBID}} {sample}_modkit_pileup_traditional.sh\n')
            f.write(f'sbatch --dependency=afterok:${{JOBID}} {sample}_mosdepth.sh\n')
            f.write(f'sbatch --dependency=afterok:${{JOBID}} {sample}_clair3.sh\n')
            
    elif not align:
        with open(f'{sample}_submitter.sh', 'w') as f:
            f.write(f'sbatch {sample}_modkit_pileup.sh\n')
            f.write(f'sbatch {sample}_modkit_pileup_traditional.sh\n')
            f.write(f'sbatch {sample}_mosdepth.sh\n')
            f.write(f'sbatch {sample}_clair3.sh\n')
      
    return


if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--sample", type=str, help="SampleID", required=True)
    parser.add_argument("-b", "--bam", type=str, help="Path to BAM", required=True)
    parser.add_argument("-g", "--genome", type=str, help="hg38 or T2T; default=hg38", default='hg38')
    parser.add_argument("--noalign", action='store_true')    

    args = parser.parse_args()
    sample = args.sample
    bam = args.bam
    ref_requested = args.genome
    no_align = args.noalign
    
    if no_align:
        align = False
    else:
        align = True
    
    main(sample, bam, ref_requested, align)    
    
    
    





















    
    
    
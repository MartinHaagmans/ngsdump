import os
import argparse

MOSDEPTH_MEM = '8G'
MOSDEPTH_THREADS = 2

SCRATCH = '/net/beegfs/scratch/mhaagmans'
MOSDEPTH = f'{SCRATCH}/programs/mosdepth'


def main(sampleID, bam, target):
    targetID = os.path.basename(target).replace('.bed', '')
    mosdepth_cmd = f'''#!/bin/sh
        #SBATCH --mem={MOSDEPTH_MEM}
        #SBATCH --cpus-per-task={MOSDEPTH_THREADS}
        #SBATCH -J {sampleID}_{targetID}_mosdepth
        #SBATCH --output=SL-%x.%j.out
        {MOSDEPTH} \\
        --no-per-base \\
        --threads {MOSDEPTH_THREADS} \\
        --by {target} \\
        {sampleID}_{targetID} \\
        {bam}
        '''
    with open(f'{sampleID}_{targetID}_mosdepth.sh', 'w') as f:
        for line in mosdepth_cmd.split('\n'):
            f.write(f'{line.lstrip()}\n')

        

if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--sample", type=str, help="SampleID", required=True)
    parser.add_argument("-b", "--bam", type=str, help="Path to BAM", required=True)
    parser.add_argument("-t", "--target", type=str, help="Path to BED", required=True)

    args = parser.parse_args()
    sample = args.sample
    bam = args.bam
    target = args.target
    main(sample, bam, target)

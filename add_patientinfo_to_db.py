import os
import sqlite3

DB = '/data/dnadiag/databases/patientinfo.sqlite'


def read_patientinfo(patientinfo):
    data = list()
    _folder, fn = os.path.split(patientinfo)
    serie = fn.split('_')[0].replace('NSX', '')
    with open(patientinfo) as f:
        for line in f:
            if line not in ['\n', '\r\n']:
                data.append('{}\t{}'.format(serie, line))
                print(serie, line)
    return data

def read_barcode_info(barcode_info):
    data = list()
    _folder, fn = os.path.split(barcode_info)
    serie = fn.split('_')[0].replace('NSX', '')
    with open(barcode_info) as f:
        for line in f:
            if line not in ['\n', '\r\n']:
                dnr, genesis, barcode = line.split()
                data.append('{}\t{}\t{}\t{}'.format(serie, dnr, barcode, genesis))
    return data

def add_info_to_db(data):
    conn = sqlite3.connect(DB)
    c = conn.cursor()    
    for line in data:
        serie, sample, sex, ff, dob, deadline = line.split()
        sex = sex.upper()
        ff = ff.upper()
        sql = """INSERT INTO patientinfo 
        (SERIE, SAMPLE, SEX, FF, DOB, DEADLINE)
        VALUES ("{}", "{}", "{}", "{}", "{}", "{}")
        """.format(serie, sample, sex, ff, dob, deadline)
        try:
            c.execute(sql)
        except sqlite3.IntegrityError as e:
            print(e)
        else:
            conn.commit()
    conn.close()
    return
    
def add_barcodes_to_db(barcodes):
    conn = sqlite3.connect(DB)
    c = conn.cursor()    
    for line in barcodes:
        serie, sample, barcode, genesis = line.split()
        sql = """INSERT INTO barcodes 
        (SERIE, SAMPLE, BARCODE, GENESIS)
        VALUES ("{}", "{}", "{}", "{}")
        """.format(serie, sample, barcode, genesis)
        try:
            c.execute(sql)
        except sqlite3.IntegrityError as e:
            print(e)
        else:
            conn.commit()
    conn.close()
    return


def main(input_fn):
    if input_fn.endswith('_patient_info.txt'):
        patients = read_patientinfo(input_fn)
        add_info_to_db(patients)
    elif input_fn.endswith('_barcodes.txt'):
        barcodes = read_barcode_info(input_fn)
        add_barcodes_to_db(barcodes)


if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, 
                        help="File met patient info", required=True)
                 
    args = parser.parse_args()
    main(args.input)